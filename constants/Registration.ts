export const formFields = [
  { name: "name", label: "Имя", placeholder: "Имя" },
  { name: "surname", label: "Фамилия", placeholder: "Фамилия" },
  { name: "company", label: "Компания", placeholder: "Компания" },
  { name: "email", label: "Рабочий email", placeholder: "Рабочий email" },
  {
    name: "phoneNumber",
    label: "Номер телефона",
    placeholder: "Номер телефона",
  },
  {
    name: "employeeCount",
    label: "Количество сотрудников в компании",
    placeholder: "Количество сотрудников в компании",
  },
  { name: "password", label: "Пароль", placeholder: "Пароль" },
  {
    name: "confirmPassword",
    label: "Подтверждение пароля",
    placeholder: "Подтверждение пароля",
  },
];
