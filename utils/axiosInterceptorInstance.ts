import axios from "axios";
import { appLocalStorage } from "@/utils/appLocalStorage";

const BASE_URL = process.env.API_BASE_URL;

const axiosInterceptorInstance = axios.create({
  baseURL: BASE_URL, // TODO: Need to place real baseURL
  headers: {
    authorization: "Basic",
  },
});

axiosInterceptorInstance.interceptors.request.use(
  async (reqConfig) => {
    const authToken = appLocalStorage.getAuthToken();
    const lng = appLocalStorage.getLng();

    if (authToken && reqConfig.headers) {
      reqConfig.params = { ...reqConfig.params, lang: lng };
      reqConfig.headers["Content-Type"] = "application/json";
      reqConfig.headers["Authorization"] = `Basic ${authToken}`;
    } else if (reqConfig.url) {
      reqConfig.headers["Authorization"] = `Basic ${authToken}`;
      reqConfig.headers["Content-Type"] = "application/json";
    }

    return reqConfig;
  },
  (error) => {
    return Promise.reject(error);
  },
);

axiosInterceptorInstance.interceptors.response.use(
  function (res) {
    return res;
  },
  function (error) {
    const errorStatus = error?.response?.status;

    if (errorStatus >= 500) {
      // TODO: Add Toaster error
    } else if (errorStatus === 403) {
      // TODO: Add Toaster error
    } else if (errorStatus === 401 || errorStatus === 409) {
      // TODO: Add Toaster error
    } else if (Number.isNaN(errorStatus)) {
      // TODO: Add Toaster error
    }

    return Promise.reject(error);
  },
);

export const api = axiosInterceptorInstance;
