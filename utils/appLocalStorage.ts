// TODO: Add tokens
const AUTH_TOKEN = "";
const LNG = "";

const getLng = () => {
  return localStorage.getItem(LNG);
};

const getAuthToken = () => {
  return localStorage.getItem(AUTH_TOKEN);
};

export const appLocalStorage = {
  getLng,
  getAuthToken,
};
