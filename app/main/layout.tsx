import { PropsWithChildren } from "react";
import Sidenav from "@/app/components/sidenav/Sidenav";

export default function MainLayout({ children }: PropsWithChildren<unknown>) {
  return (
    <div className="flex">
      <Sidenav />
      {children}
    </div>
  );
}
