import {
  FormControl,
  FormField,
  FormItem,
  FormLabel,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { UseFormReturn } from "react-hook-form";
import { FC } from "react";

type Props = {
  form: UseFormReturn<any>;
  name: string;
  label: string;
  placeholder: string;
};

export const FormFieldComponent: FC<Props> = ({
  form,
  name,
  label,
  placeholder,
}) => {
  return (
    <FormField
      control={form.control}
      name={name}
      render={({ field }) => (
        <FormItem className="m-3">
          <FormLabel>{label}</FormLabel>
          <FormControl>
            <Input
              className="border-[#D1D1D1] rounded-none"
              placeholder={placeholder}
              {...field}
            />
          </FormControl>
        </FormItem>
      )}
    />
  );
};
