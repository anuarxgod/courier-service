import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { Form } from "@/components/ui/form";
import { z } from "zod";
import { Button } from "@/components/ui/button";
import { FormFieldComponent } from "@/app/components/formField/FormField";
import styles from "./RegistrationForm.module.scss";
import { formFields } from "@/constants/Registration";

const formSchema = z
  .object({
    name: z.string().min(2, {
      message: "Name must be at least 2 characters.",
    }),
    surname: z.string().min(2, {
      message: "Surname must be at least 2 characters.",
    }),
    company: z.string().array().nonempty({
      message: "Surname must be at least 2 characters.",
    }),
    email: z.string().email({
      message: "Invalid email format.",
    }),
    phoneNumber: z.string().regex(/^\+7\d{10}$/, {
      message: "Invalid phone number format.",
    }),
    employeeCount: z.string().min(1, {
      message: "Employee count must be selected.",
    }),
    password: z.string().min(8, {
      message: "Password must be at least 8 characters long.",
    }),
    confirmPassword: z.string().min(8, {
      message: "Password must be at least 8 characters long.",
    }),
  })
  .superRefine(({ confirmPassword, password }, ctx) => {
    if (confirmPassword !== password) {
      ctx.addIssue({
        code: "custom",
        message: "The passwords did not match",
      });
    }
  });

export const RegistrationForm = () => {
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: Object.fromEntries(
      formFields.map((field) => [field.name, ""]),
    ),
  });

  function onSubmit(values: z.infer<typeof formSchema>) {
    console.log(values);
  }

  const renderFormFields = () => {
    const halfLength = Math.ceil(formFields.length / 2);
    const firstHalf = formFields.slice(0, halfLength);
    const secondHalf = formFields.slice(halfLength);

    return (
      <div className="flex flex-wrap gap-10 mt-10">
        <div>
          {firstHalf.map(({ name, label, placeholder }) => (
            <FormFieldComponent
              key={name}
              form={form}
              name={name}
              label={label}
              placeholder={placeholder}
            />
          ))}
        </div>
        <div>
          {secondHalf.map(({ name, label, placeholder }) => (
            <FormFieldComponent
              key={name}
              form={form}
              name={name}
              label={label}
              placeholder={placeholder}
            />
          ))}
        </div>
      </div>
    );
  };

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <>{renderFormFields()}</>
        <Button className={styles.submitButton} type="submit">
          Зарегистрироваться
        </Button>
      </form>
    </Form>
  );
};
