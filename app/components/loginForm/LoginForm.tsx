import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { FormFieldComponent } from "@/app/components/formField/FormField";
import { Form } from "@/components/ui/form";
import { Button } from "@/components/ui/button";
import { loginFormFields } from "@/constants/Login";

const formSchema = z.object({
  email: z.string().min(2, {
    message: "Name must be at least 2 characters.",
  }),
  password: z.string().min(2, {
    message: "Surname must be at least 2 characters.",
  }),
});

export default function LoginForm() {
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: Object.fromEntries(
      loginFormFields.map((field) => [field.name, ""]),
    ),
  });

  function onSubmit(values: z.infer<typeof formSchema>) {
    console.log(values);
  }

  const renderFormFields = () => {
    return (
      <>
        {loginFormFields.map(({ name, label, placeholder }) => (
          <FormFieldComponent
            key={name}
            form={form}
            name={name}
            label={label}
            placeholder={placeholder}
          />
        ))}
      </>
    );
  };

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <>{renderFormFields()}</>
        <Button
          className="bg-[#4743E0] rounded-none float-right mt-10"
          type="submit"
        >
          Войти
        </Button>
      </form>
    </Form>
  );
}
