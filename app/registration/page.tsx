"use client";

import Image from "next/image";
import styles from "./RegistrationPage.module.scss";
import registrationPicture from "@/public/assets/registration/registrationPicture.png";
import separateLine from "@/public/assets/registration/separate-line.png";
import { RegistrationForm } from "@/app/components/registrationForm/RegistrationForm";

export default function Registration() {
  return (
    <div className="flex">
      <div className="flex items-center flex-col mt-24">
        <div className="flex justify-center items-center">
          <h1 className="text-[#42505C] text-[40px] font-bold w-[20%]">
            Введите данные
          </h1>
          <p className={styles.description}>
            Добро пожаловать в будущее логистики! Нам нужна некоторая базовая
            информация для создания вашего аккаунта.{" "}
            <span className="text-[#0019FF] cursor-pointer">
              Вот наши условия использования.
            </span>
          </p>
        </div>
        <Image className="mt-10" src={separateLine} alt="" />
        <RegistrationForm />
      </div>
      <Image className="mt-[71px]" src={registrationPicture} alt="" />
    </div>
  );
}
