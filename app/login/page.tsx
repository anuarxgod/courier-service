"use client";

import LoginForm from "@/app/components/loginForm/LoginForm";

export default function Login() {
  return (
    <div className="flex justify-center items-center">
      <div className="mt-[250px]">
        <div className="flex items-start ml-2 flex-col">
          <h1 className="text-[#42505C] text-[45px] font-bold w-[20%]">
            Войти
          </h1>
          <span className="w-[60%] block">
            Пожалуйста, введите свои данные в поля ниже, чтобы войти в свой
            аккаунт
          </span>
        </div>
        <LoginForm />
      </div>
    </div>
  );
}
